snmpv3
=========

### CENTOS only at this stage 

Disables SNMP V1 and 2c due to security concerns and configures an authenticated user for SNMP V3. If net-snmp isn't installed it will skip the host. Reconfigure any snmp queries to utilise v3 authenticated settings.

eg;

Cacti;


![cacti snmpv3](meta/snmpv3_cacti_settings.PNG)

CLI;

````bash
snmpwalk -u <snmpv3user> -A <snmpv3authpass> -a SHA -X <snmpv3privpass> -x AES -l authPriv <server> -v3
````

Role Variables
--------------

* Vars to be changed in vars/main.yml
    * __snmpv3user__ : username
    * __snmpv3authpass__ : password for authentication
    * __snmpv3privpass__ : privacy password for encrypting traffic

Example Playbook
----------------
````yaml
- hosts: servers
  roles:
     - role: snmpv3
````

License
-------

BSD

Author Information
------------------
sotirk (kristians@gmail.com)
